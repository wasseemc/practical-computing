# My Project's README

Whole Genome Assembly utilizing Nanopore or PacBio sequencing
## Installing poretools:
(This step is necessary to take base-called FAST5 files and convert them into FASTQ files.)
```
brew tap homebrew/science

brew install hdf5 

pip install cython 

pip install numpy 

git clone https://github.com/arq5x/poretools 

cd poretools

sudo python setup.py install 
```
(had to fix a python version difference by adding parenthesis after print)
```
nano setup.py 
sudo python setup.py install
```
 
## Installing minimap and miniasm (requiring gcc and zlib):
```
git clone https://github.com/lh3/minimap && 
```
or
```
git clone https://github.com/lh3/minimap
cd minimap
make
```
then:
```
git clone https://github.com/lh3/minimap2 && 
```
or
```
git clone https://github.com/lh3/minimap2
cd minimap2
make
```
finally:
```
git clone https://github.com/lh3/miniasm && 
```
or
```
git clone https://github.com/lh3/miniasm
cd miniasm
make
```
(They should work either way, minimap had an updated version called minimap2 so I've included both just in case.)

### Downloading sample PacBio data from the PBcR website
```
curl http://www.cbcb.umd.edu/software/PBcR/data/selfSampleData.tar.gz | tar zxf -

ln -s selfSampleData/pacbio_filtered.fastq reads.fq
```
### Overlap:
```
minimap/minimap -Sw5 -L100 -m0 -t8 reads.fq reads.fq | gzip -1 > reads.paf.gz
```
### Layout:
```
miniasm/miniasm -f reads.fq reads.paf.gz > utg.gfa
```
### Conversion to FASTA:
```
awk '/^S/{print ">"$2"\n"$3}' utg.gfa > utg.fa
```

### Downloading E. coli K-12 sequence:
```
curl NC_000913.fa 'https://www.ncbi.nlm.nih.gov/sviewer/viewer.cgi?sendto=on&db=nuccore&dopt=fasta&val=556503834'
```
### Mapping assembly to reference:
```
minimap/minimap NC_000913.fa utg.fa | miniasm/minidot - > utg.eps
```
Since the data set was very simple, minimap and miniasm were actually able to assemble the entire genome. 

Given a larger data set and genome size Canu pipeline would need to be utilized to assemble the entire genome.
## Installing Canu
```
brew install brewsci/bio/canu 
```
Java download (both jre and jdk versions) required to run:
```
canu -d ecoli-pacbio -p ecoli genomeSize=4.8m -pacbio-raw reads.paf.gz 
```